﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnparentedMessageBox
{
    /// <summary>
    ///   http://stackoverflow.com/q/195593/429091
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // http://stackoverflow.com/questions/3705648/how-to-bind-one-control-to-another#comment39336290_3705684
            checkBoxSetOwner.DataBindings.Add("Enabled", checkBoxShowNoDialog, "Checked");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            var message = "hi";
            var owner = checkBoxExplicitParent.Checked ? this : null;
            if (checkBoxSeparateThread.Checked)
                new System.Threading.Thread(
                    () =>
                    {
                        showMessage(owner, message);
                        // Guard against calling Invoke() if this has been Disposed().
                        if (InvokeRequired)
                            Invoke((Action)(() => timer1.Start()));
                    }).Start();
            else
            {
                showMessage(owner, message);
                timer1.Start();
            }
        }

        private void showMessage(IWin32Window owner, string message)
        {
            if (owner != null)
                MessageBox.Show(owner, message);
            else
                MessageBox.Show(message);
        }

        private void buttonSpawnChild_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            if (checkBoxShowNoDialog.Checked)
            {
                var f =  new Form1();
                if (checkBoxSetOwner.Checked)
                    f.Owner = this;
                f.FormClosed +=
                    (_sender, _e) =>
                    {
                        f.Dispose();
                        timer1.Start();
                    };
                f.Show();
            }
            else
            {
                using (var f = new Form1())
                    f.ShowDialog();
                timer1.Start();
            }
        }
    }
}
