﻿namespace UnparentedMessageBox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.checkBoxExplicitParent = new System.Windows.Forms.CheckBox();
            this.checkBoxSeparateThread = new System.Windows.Forms.CheckBox();
            this.buttonSpawnChild = new System.Windows.Forms.Button();
            this.checkBoxShowNoDialog = new System.Windows.Forms.CheckBox();
            this.checkBoxSetOwner = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 4000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // checkBoxExplicitParent
            // 
            this.checkBoxExplicitParent.AutoSize = true;
            this.checkBoxExplicitParent.Location = new System.Drawing.Point(24, 15);
            this.checkBoxExplicitParent.Name = "checkBoxExplicitParent";
            this.checkBoxExplicitParent.Size = new System.Drawing.Size(93, 17);
            this.checkBoxExplicitParent.TabIndex = 0;
            this.checkBoxExplicitParent.Text = "Explicit &Parent";
            this.checkBoxExplicitParent.UseVisualStyleBackColor = true;
            // 
            // checkBoxSeparateThread
            // 
            this.checkBoxSeparateThread.AutoSize = true;
            this.checkBoxSeparateThread.Location = new System.Drawing.Point(18, 40);
            this.checkBoxSeparateThread.Name = "checkBoxSeparateThread";
            this.checkBoxSeparateThread.Size = new System.Drawing.Size(106, 17);
            this.checkBoxSeparateThread.TabIndex = 1;
            this.checkBoxSeparateThread.Text = "Separate &Thread";
            this.checkBoxSeparateThread.UseVisualStyleBackColor = true;
            // 
            // buttonSpawnChild
            // 
            this.buttonSpawnChild.Location = new System.Drawing.Point(18, 82);
            this.buttonSpawnChild.Name = "buttonSpawnChild";
            this.buttonSpawnChild.Size = new System.Drawing.Size(75, 23);
            this.buttonSpawnChild.TabIndex = 2;
            this.buttonSpawnChild.Text = "&Spawn Child";
            this.buttonSpawnChild.UseVisualStyleBackColor = true;
            this.buttonSpawnChild.Click += new System.EventHandler(this.buttonSpawnChild_Click);
            // 
            // checkBoxShowNoDialog
            // 
            this.checkBoxShowNoDialog.AutoSize = true;
            this.checkBoxShowNoDialog.Location = new System.Drawing.Point(99, 88);
            this.checkBoxShowNoDialog.Name = "checkBoxShowNoDialog";
            this.checkBoxShowNoDialog.Size = new System.Drawing.Size(196, 17);
            this.checkBoxShowNoDialog.TabIndex = 3;
            this.checkBoxShowNoDialog.Text = "Use Show() instead of Show&Dialog()";
            this.checkBoxShowNoDialog.UseVisualStyleBackColor = true;
            // 
            // checkBoxSetOwner
            // 
            this.checkBoxSetOwner.AutoSize = true;
            this.checkBoxSetOwner.Checked = true;
            this.checkBoxSetOwner.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSetOwner.Location = new System.Drawing.Point(98, 113);
            this.checkBoxSetOwner.Name = "checkBoxSetOwner";
            this.checkBoxSetOwner.Size = new System.Drawing.Size(76, 17);
            this.checkBoxSetOwner.TabIndex = 4;
            this.checkBoxSetOwner.Text = "Set &Owner";
            this.checkBoxSetOwner.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.checkBoxSetOwner);
            this.Controls.Add(this.checkBoxShowNoDialog);
            this.Controls.Add(this.buttonSpawnChild);
            this.Controls.Add(this.checkBoxSeparateThread);
            this.Controls.Add(this.checkBoxExplicitParent);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox checkBoxExplicitParent;
        private System.Windows.Forms.CheckBox checkBoxSeparateThread;
        private System.Windows.Forms.Button buttonSpawnChild;
        private System.Windows.Forms.CheckBox checkBoxShowNoDialog;
        private System.Windows.Forms.CheckBox checkBoxSetOwner;
    }
}

